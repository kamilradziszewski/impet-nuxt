# Website for ["IMPET – Skład opału"](https://impet-bialystok.com)

Features:

- [Nuxt.js](https://nuxtjs.org/)
- [Sanity (CMS)](https://www.sanity.io/)
- [GraphQL/Apollo](https://www.apollographql.com/)
- [Buefy](https://buefy.org/)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
