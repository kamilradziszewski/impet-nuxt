module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title:
      'IMPET Białystok – skład opału | Węgiel ekogroszek, groszek, orzech, kostka',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'P.U.H. IMPET - skład opału w Białymstoku. Oferujemy bogaty asortyment węgla polskiego i importowanego. Posiadamy 25-letnie doświadczenie w handlu opałem.'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['~/assets/scss/main.scss'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-fragment',
    { src: '~~node_modules/vue-rellax/lib/nuxt-plugin', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
    [
      'nuxt-buefy',
      {
        defaultIconPack: 'fas',
        materialDesignIconsHRef:
          'https://use.fontawesome.com/releases/v5.4.1/css/all.css'
      }
    ],
    '@nuxtjs/apollo',
    '@aceforth/nuxt-optimized-images'
  ],

  optimizedImages: {
    optimizeImages: true,
    optimizeImagesInDev: true,
    svgo: {
      plugins: [{ removeDimensions: true }]
    }
  },

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint:
          'https://9i2x97jk.api.sanity.io/v1/graphql/production/default'
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
